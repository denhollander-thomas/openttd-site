'use strict';

function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function parseScoreData(gameNumber, callback) {
  httpGetAsync("save_" + gameNumber + ".txt", (text) => {
    const values = [];
    const lines = text.split("\n");
    const companies = lines[0]
      .split("\t")
      .map((d) => d.trim())
      .filter((d) => d !== '');
    for (const line of lines.slice(1)) {
      if (line.trim() === '')
        break;

      const items = line.split("\t");
      const date = parseInt(items[0]);
      const vals = [];
      for (let i = 1; i < items.length; i++) {
        if (items[i].trim() === '')
          break;
        vals.push(parseInt(items[i]));
      }
      values.push({date, vals});
    }
    callback({
      gameNumber,
      companies,
      values
    });
  });
}

let gameNumber;
let log = false;
let currentGame = undefined;

document.addEventListener("DOMContentLoaded", () => {
  httpGetAsync("game_number.txt", (number) => {
    gameNumber = parseInt(number.trim(), 10);
    const scoreTabel = document.getElementById("scoretabel");

    let done = 0;
    let games = [];
    for (let i = 0; i < gameNumber + 1; i++) {
      parseScoreData(i, (data) => {
        done++;
        games.push(data);
        if (done === gameNumber + 1) {
          for (const game of games) {
            const row = document.createElement('tr');

            const gameNo = document.createElement('td');
            gameNo.innerHTML = "#" + (game.gameNumber + 1);
            row.appendChild(gameNo);

            const companyCell = document.createElement('td');
            companyCell.innerHTML = game.companies.join(', ');
            row.appendChild(companyCell);

            const gameWinner = document.createElement('td');
            if (game.gameNumber === gameNumber) {
              gameWinner.innerHTML = '(Currently running)'
            } else {
              let maxIndex = 0;
              const endCompanyValues = game.values[game.values.length - 1].vals;
              for (let i = 1; i < game.companies.length; i++) {
                if (endCompanyValues[i] > endCompanyValues[maxIndex])
                  maxIndex = i;
              }
              gameWinner.innerHTML = game.companies[maxIndex];
            }
            row.appendChild(gameWinner);

            const graphLink = document.createElement('td');
            graphLink.innerHTML = '<a href="#" class="button">Grafiek</a>'
            graphLink.addEventListener('click', () => {
              currentGame = game;
              drawGraph();
            })
            row.appendChild(graphLink);

            scoreTabel.appendChild(row);
          }

          const logButton = document.getElementById('button_log');
          logButton.addEventListener('click', () => {
            log = true;
            drawGraph();
          });

          const linButton = document.getElementById('button_lin');
          linButton.addEventListener('click', () => {
            log = false;
            drawGraph();
          })
        }
      })
    }
  });
});

function drawGraph() {

  const game = currentGame;

  if (typeof game === 'undefined')
    return;

  const svg = d3.select('svg');
  const margin = {top: 20, right: 20, bottom: 30, left: 50};
  const bbox = svg.node().getBoundingClientRect();
  const width = bbox.width - margin.left - margin.right;
  const height = bbox.height - margin.top - margin.bottom;

  const numberOfCompanies = d3.max(game.values, (d) => d.vals.length);
  const data = [];
  let minDate;
  let maxDate;
  let maxValue;
  for (let i = 0; i < numberOfCompanies; i++) {
    data.push(game.values.map((d) => {
      const date = new Date(d.date);
      const value = typeof d.vals[i] === 'undefined' ? 1 : Math.max(parseInt(d.vals[i]), 1);

      if (typeof minDate === 'undefined' || minDate > date)
        minDate = date;
      if (typeof maxDate === 'undefined' || maxDate < date)
        maxDate = date;
      if (typeof maxValue === 'undefined' || maxValue < value)
        maxValue = value;

      return { date, value };
    }));
  }

  const x = d3.scaleTime()
  .rangeRound([0, width])
  .domain([minDate, maxDate]);

  const y = (log ? d3.scaleLog() : d3.scaleLinear())
  .rangeRound([height, 0])
  .domain([1, maxValue]);

  const line = d3.line()
    .x((d) => x(d.date))
    .y((d) => y(d.value))
    .curve(d3.curveMonotoneX);

  svg.select('#graph-group').remove();

  const g = svg.append("g")
    .attr('id', 'graph-group')
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .attr('fill', '#fff');

  g.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .call(d3.axisBottom(x));

  g.append('g')
    .call(d3.axisLeft(y))
    .append('text')
    .attr('fill', '#fff')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '0.7em')
    .attr('text-anchor', 'end')
    .text('Bedrijfswaarde (£)');

  const colorScale = d3.scaleOrdinal(d3.schemeCategory10).domain(d3.range(0, 9));

  for (let i = 0; i < numberOfCompanies; i++)
    g.append('path')
      .datum(data[i])
      .attr('fill', 'none')
      .attr("stroke", colorScale(i))
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1.5)
      .attr("d", line);

    svg.selectAll('.tick').attr('stroke', '#fff');
    svg.selectAll('.tick').selectAll('line').attr('stroke', '#fff');
    svg.selectAll('.domain').attr('stroke', '#fff');

    const legend = document.getElementById('legend');
    legend.innerHTML = '';
    for (let i = 0; i < numberOfCompanies; i++) {
      const listElement = document.createElement('li');
      listElement.innerHTML = game.companies[i];
      listElement.style.color = colorScale(i);
      legend.appendChild(listElement);
    }
}
